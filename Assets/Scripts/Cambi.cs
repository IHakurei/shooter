﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cambi : MonoBehaviour {
    public GameObject m1;
    public GameObject m2;
    // Use this for initialization

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.tag == "Player")
        {
            Debug.Log("cambi");
            m1.SetActive(false);
            m2.SetActive(true);
        }
    }
  
}
