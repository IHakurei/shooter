﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Menu : MonoBehaviour {

	public void Inici()
    {
        SceneManager.LoadScene("Prototype");
    }

    public void Exit()
    {
        Application.Quit();
    }
    public void Torna()
    {
        SceneManager.LoadScene("Menu");
    }
    public void Credits()
    {
        SceneManager.LoadScene("Credits");
    }
}
