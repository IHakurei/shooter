﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KnifeRemove : MonoBehaviour {
    float lifespan = 3.0f;
    public AudioSource ad;
    public void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        lifespan -= Time.deltaTime;
        transform.Translate(new Vector3(0,0,30) * Time.deltaTime);
        transform.Rotate(0,0,90, Space.Self);
        if (lifespan <= 0)
        {
            Explode();
        }
    }

    void OnCollisionEnter(Collision collision)
    {

        if (collision.gameObject.tag == "Enemy")
        {
            //collision.gameObject.tag = "Untagged";
            ad.Play();
        
            Explode();
        }
    }

    void Explode()
    {

        Destroy(gameObject);
    }
}
