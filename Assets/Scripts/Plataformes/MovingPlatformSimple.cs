﻿using UnityEngine;
using System.Collections;

public class MovingPlatformSimple : MonoBehaviour {
	
	public Transform startPos;
	public Transform secondPos;
	public Transform thirdPos;
	public Transform endPos;

	private Vector3 direction;
	private Transform destination;
	
	public float speed = 1.0f;

	
	bool dir = false;


	// Update is called once per frame
	void Update () {
		if (dir) {
			
			transform.position = Vector3.MoveTowards(transform.position,
				endPos.position,
				Time.deltaTime * speed);

			// reached it?
			if (transform.position == endPos.position)
				dir = !dir; 
		} else {
			// go closer to the left one
			transform.position = Vector3.MoveTowards(transform.position,
				startPos.position,
				Time.deltaTime * speed);

			// reached it?
			if (transform.position == startPos.position)
				dir = !dir; 
		}
	}
}