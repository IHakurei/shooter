﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class MovingPlatformComplex : MonoBehaviour {

	public Transform movingPlatform;
	public Transform position1;
	public Transform position2;
	public Transform position3;
	public Transform position4;
	public Vector3 newPosition;
	public float smooth;
	public float resetTime;

	void Start()
	{
		ChangeTarget();
	}


	void FixedUpdate()
	{
		movingPlatform.position = Vector3.Lerp (movingPlatform.position, newPosition, smooth * Time.deltaTime);
	}

	void ChangeTarget(){
		if(movingPlatform.position == position1.position){
			newPosition = position2.position;
		}
		else if(movingPlatform.position == position2.position){
			newPosition = position3.position;
		}
		else if(movingPlatform.position == position3.position){
			newPosition = position4.position;
		}
		else if(movingPlatform.position == position4.position){
			newPosition = position1.position;
		}
		Invoke ("Change Target", resetTime);
	}

    }