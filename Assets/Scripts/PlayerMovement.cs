﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlayerMovement : MonoBehaviour {

	private CharacterController controller;
	private Vector2 axis;
    private float forceToGround = Physics.gravity.y;
    private bool jump;

    public float speed;
	public Vector3 moveDirection;
    public Animator anim;
	public float jumpSpeed;
	public float gravityMagnitude = 1.0f;
	public Slider healthBar;

	void Start ()
	{
		controller = GetComponent<CharacterController>();
        anim = GetComponentInChildren<Animator>();
    }

	void Update ()
	{
     

        {

        }
		if(controller.isGrounded && !jump)
		{
			moveDirection.y = forceToGround;
            anim.SetBool("jump", false);
        }
		else
		{
			jump = false;
           
            moveDirection.y += Physics.gravity.y * gravityMagnitude * Time.deltaTime;
		}

		Vector3 transformDirection = axis.x * transform.right + axis.y * transform.forward;

		moveDirection.x = transformDirection.x * speed;
		moveDirection.z = transformDirection.z * speed;
        if (moveDirection.z >= 0.001)
            anim.SetBool("walk", true);
        if (moveDirection.x >= 0.001)
            anim.SetBool("walk", true);

        else if (moveDirection.x <= 0.001)
        {
            anim.SetBool("walk", false);
            
        }
        else if (moveDirection.z <= 0.001)
        {
            anim.SetBool("walk", false);

        }
        controller.Move(moveDirection * Time.deltaTime);

		if(healthBar.value == 0){

			GameOver();
		}

    
    }
 
    public void SetAxis(Vector2 inputAxis)
	{
		axis = inputAxis;
       
	}

	public void StartJump()
	{
		if(!controller.isGrounded) return;
        anim.SetBool("jump", true);
		moveDirection.y = jumpSpeed;
		jump = true;
	}

	private void OnTriggerEnter(Collider hit)
    {
		if (hit.gameObject.tag == "Weapon") {
			
			Debug.Log("Hit");
			healthBar.value -= 10;
		}

        if (hit.gameObject.tag == "Enemy")
        {

            Debug.Log("Hit");
            healthBar.value -= 10;
        }
        else if(hit.gameObject.tag == "WeaponMid"){

			healthBar.value -= 20;
		}
		else if(hit.gameObject.tag == "Super"){

			healthBar.value -=30;
		}
    }

	private void GameOver(){

			SceneManager.LoadScene("Over");
	}
}
